`npm install `

- the install won't succeed, it will throw and error:
"ERROR in ENOENT: no such file or directory, open '~/node_modules/ng4-loading-spinner/ng4-loading-spinner.js'"

- add the file named 'ng4-loading-spinner.js' that is included in the "includes" folder, to '~/node_modules/ng4-loading-spinner/'

- re-install. should now compile

- click on the "user icon" in the id column to reveal the details about a bank branch

- have to work on testing, as none are passing at the moment.

