import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable, isDevMode } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { throwError } from "rxjs/internal/observable/throwError";
import { catchError, retry } from "rxjs/operators";
import { environment } from "../../environments/environment.prod";
import { IBranch } from "../interfaces/ibranch.interface";

@Injectable()
export class HalifaxApiService {
  branches: any[];

  constructor(private http: HttpClient) {
    this.branches = this.branches;
  }

  private httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    })
  };

  // GET by Id
  getBranch(id): Observable<IBranch> {
    return this.http
      .get<IBranch>(environment.getBranchesUrl, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  getBranches(): Observable<IBranch> {
    return this.http
      .get<IBranch>(environment.getBranchesUrl, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  // Error handling
  handleError(error) {
    let errorMessage = "";
    error.error instanceof ErrorEvent
      ? // Get client-side error
        (errorMessage = error.error.message)
      : // Get server-side error
        (errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`);

    return throwError(errorMessage);
  }
}
