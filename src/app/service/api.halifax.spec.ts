import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { inject, TestBed } from "@angular/core/testing";
import { environment } from "../../environments/environment.prod";
import { HalifaxApiService } from "./api.halifax";

describe("ApiService", () => {
  const mockErrorResponse = { status: 400, statusText: "Bad Request" };
  const data = "Invalid request parameters";
  const mockResponse = {
    status: "200"
  };
  let apiService: HalifaxApiService;
  let httpTestingController: HttpTestingController;
  let response: any;
  // tslint:disable-next-line: prefer-const
  let errResponse: any;

  beforeEach(() => {
    apiService = TestBed.get(apiService);
    httpTestingController = TestBed.get(HttpTestingController);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HalifaxApiService]
    });

    it(`should log an error to the console on error on getBranches()`, inject(
      [HalifaxApiService, HttpTestingController],
      // tslint:disable-next-line: no-shadowed-variable
      (apiService: HalifaxApiService, httpMock: HttpTestingController) => {
        apiService
          .getBranches()
          .subscribe(res => (response = res), err => (errResponse = err));

        httpTestingController
          .expectOne(`${environment.getBranchesUrl}`)
          .flush(data, mockErrorResponse);

        expect(errResponse).toBe(data);
      }
    ));

    it(`should be created`, inject(
      [HalifaxApiService],
      (service: HalifaxApiService) => {
        expect(service).toBeTruthy();
      }
    ));

    afterEach(() => {
      httpTestingController.verify();
    });
  });
});
