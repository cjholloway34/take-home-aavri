export interface IBranch {
  meta?: IMeta;
  data?: IDataEntity[] | null;
}
export interface IMeta {
  LastUpdated: string;
  TotalResults: number;
  Agreement: string;
  License: string;
  TermsOfUse: string;
}
export interface IDataEntity {
  Brand?: IBrandEntity[] | null;
}
export interface IBrandEntity {
  [x: string]: any;
  BrandName: string;
  IBranch?: IBranchEntity[] | null;
}
export interface IBranchEntity {
  Identification: string;
  SequenceNumber: string;
  Name: string;
  Type: string;
  CustomerSegment?: string[] | null;
  ServiceAndFacility?: string[] | null;
  Accessibility?: string[] | null;
  OtherServiceAndFacility?: IOtherServiceAndFacilityEntity[] | null;
  IAvailability: IAvailability;
  ContactInfo?: IContactInfoEntity[] | null;
  IPostalAddress: IPostalAddress;
}
export interface IOtherServiceAndFacilityEntity {
  Code: string;
  Name: string;
  Description: string;
}
export interface IAvailability {
  IStandardIAvailability: IStandardIAvailability;
}
export interface IStandardIAvailability {
  Day?: IDayEntity[] | null;
}
export interface IDayEntity {
  Name: string;
  OpeningHours?: IOpeningHoursEntity[] | null;
}
export interface IOpeningHoursEntity {
  OpeningTime: string;
  ClosingTime: string;
}
export interface IContactInfoEntity {
  ContactType: string;
  ContactContent: string;
}
export interface IPostalAddress {
  AddressLine?: string[] | null;
  TownName?: string | null;
  CountrySubDivision?: string[] | null;
  Country: string;
  PostCode: string;
  IGeoLocation: IGeoLocation;
}
export interface IGeoLocation {
  IGeographicCoordinates: IGeographicCoordinates;
}
export interface IGeographicCoordinates {
  Latitude: string;
  Longitude: string;
}
