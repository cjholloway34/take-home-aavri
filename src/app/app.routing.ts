import { RouterModule, Routes } from "@angular/router";
import { BranchesListComponent } from "./banks/branches-list/branches-list.component";
import { BranchDetailsModalComponent } from "./modals/branch-details-modal/branch-details-modal.component";

const routes: Routes = [
  { path: "branches-list", component: BranchesListComponent },
  { path: "", redirectTo: "/branches-list", pathMatch: "full" }
];

export const routing = RouterModule.forRoot(routes);
