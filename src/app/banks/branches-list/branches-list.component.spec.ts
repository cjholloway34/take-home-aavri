import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
import { FilterPipeModule } from "ngx-filter-pipe";
import { LoggerConfig } from "ngx-logger";
import { BranchesListComponent } from "./branches-list.component";

describe("BranchesListComponent", () => {
  let component: BranchesListComponent;
  let fixture: ComponentFixture<BranchesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BranchesListComponent],
      imports: [
        FormsModule,
        FilterPipeModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [LoggerConfig, Ng4LoadingSpinnerService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create branches list component", () => {
    expect(component).toBeTruthy();
  });
});
