import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { IBranch } from "../../interfaces/ibranch.interface";
import { BranchDetailsModalComponent } from "../../modals/branch-details-modal/branch-details-modal.component";
import { HalifaxApiService } from "../../service/api.halifax";

import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
import {
  LoggerConfig,
  NGXLogger,
  NGXLoggerHttpService,
  NGXMapperService
} from "ngx-logger";

@Component({
  selector: "app-branches-list",
  templateUrl: "./branches-list.component.html",
  styleUrls: ["./branches-list.component.css"],
  providers: [
    HalifaxApiService,
    LoggerConfig,
    NGXLogger,
    NGXLoggerHttpService,
    NGXMapperService
  ]
})
export class BranchesListComponent implements OnInit {
  branches: any[];
  cityFilter: any = { PostalAddress: { TownName: "" } };
  title = "Halifax Branches";
  closeResult: string;
  modalOptions: NgbModalOptions;
  items = [];
  page = 1;
  pageSize = 30;
  collectionSize: number;
  isLoading = true;

  constructor(
    private router: Router,
    private apiService: HalifaxApiService,
    private modalService: NgbModal,
    private spinnerService: Ng4LoadingSpinnerService,
    private logger: NGXLogger
  ) {
    this.modalOptions = {
      backdrop: "static",
      backdropClass: "customBackdrop"
    };
  }

  ngOnInit() {
    this.spinnerService.show();
    this.apiService.getBranches().subscribe(
      async result => {
        const data = await result.data[0].Brand[0].Branch;

        this.branches = data;
        this.collectionSize = this.branches.length;
        this.isLoading = false;
        this.spinnerService.hide();
      },
      error => {
        throw new Error("No Bank Branches were returned!");
      }
    );
  }

  openBranchDetailsModal(branch): void {
    this.logger.log(branch);
    const modalRef = this.modalService.open(BranchDetailsModalComponent, {
      centered: true
    });
    modalRef.componentInstance.dummyImage = "https://lorempixel.com/250/250/";
    modalRef.componentInstance.postalAddress = branch.PostalAddress.AddressLine;
    modalRef.componentInstance.name = branch.Name;
    modalRef.componentInstance.phoneNumber =
      branch.ContactInfo[0].ContactContent;
    modalRef.componentInstance.city = branch.PostalAddress.TownName;
    modalRef.componentInstance.subDivision =
      branch.PostalAddress.CountrySubDivision;
    modalRef.componentInstance.postalCode = branch.PostalAddress.PostalCode;
    modalRef.componentInstance.country = branch.PostalAddress.Country;
  }

  back() {
    this.router.navigate(["branches-list"]);
  }
}
