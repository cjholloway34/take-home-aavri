import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

export const environment = {
  production: false,
  getBranchesUrl: "https://api.halifax.co.uk/open-banking/v2.2/branches"
};
